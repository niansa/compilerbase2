#include "parser.hpp"

#include <fstream>
#include <sstream>
#include <string>



std::string slurp(std::ifstream& in) {
    std::ostringstream sstr;
    sstr << in.rdbuf();
    return sstr.str();
}

int main(int argc, char **argv) {
    std::ifstream file(argv[1]);
    auto fileData = slurp(file);
    Compiler::Parser parser;
    auto fileDataIterator = fileData.cbegin();
    parser.parse(fileDataIterator, fileData.cend());
}
