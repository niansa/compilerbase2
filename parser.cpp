#include "parser.hpp"

#include <iostream>
#include <string>
#include <string_view>



namespace Compiler {
char Parser::getClosingBrace(char c) {
    switch (c) {
    case '(': return ')';
    case '{': return '}';
    case '[': return ']';
    case '<': return '>';
    default: return 0;
    }
}

std::string_view Parser::getRawToken(std::string::const_iterator& it, std::string::const_iterator end) {
    while (*it == ' ' || *it == '\t' || *it == '\n' || *it == '\r') {
        it++;
        if (it >= end) {
            return "";
        }
    }
    auto start = it.base();
    if (!isalnum(*it)) {
        it++;
        return std::string_view{start, 1};
    } else {
        while (isalnum(*it) && it < end) {
            it++;
        }
        return std::string_view{start, std::string_view::size_type(it.base() - start)};
    }
}

std::string_view Parser::getString(std::string::const_iterator& it, std::string::const_iterator end, char terminator) {
    auto start = it.base();
    unsigned escapeNext = 0;
    while (true) {
        it++;
        if (it >= end) {
            throw SyntaxError("Reached end while searching for terminating '\"'");
        } else if (*it == '\\') {
            escapeNext = 2;
        } else if (*it == terminator) {
            if (!escapeNext) {
                break;
            }
        }
        if (escapeNext) {
            escapeNext--;
        }
    }
    it++;
    return std::string_view{start, std::string_view::size_type(it.base() - start - 1)};
}

Parser::Token Parser::getToken(std::string::const_iterator& it, std::string::const_iterator end) {
    Token fres;
    fres.data = getRawToken(it, end);
    if (fres.data[0] == '"') {
        fres.type = Token::Type::string;
        fres.data = getString(it, end);
    } else if (!isalnum(fres.data[0])) {
        fres.type = Token::Type::character;
    } else {
        fres.type = Token::Type::identifier;
    }
    return fres;
}

std::string Parser::Token::getHumanReadable() const {
    if (type == Type::string) {
        return '"' + std::string(data) + '"';
    } else {
        return std::string(data);
    }
}

std::string Parser::SimpleASTItem::getHumanReadable(int depth) const {
    std::string fres;
    auto applyDepth = [&] () {
        if (depth > 0) {
            for (unsigned i = 0; i != depth; i++) {
                fres.push_back(' ');
            }
        }
    };
    // Add raw token
    applyDepth();
    fres.append(token.getHumanReadable());
    // Add all children
    if (!children.empty()) {
        fres.push_back('\n');
        for (const auto& child : children) {
            fres.append(child.getHumanReadable(depth + 1));
            fres.push_back('\n');
        }
        // Add closing brace
        auto closingBrace = getClosingBrace(getCharacter());
        if (closingBrace) {
            applyDepth();
            fres.push_back(closingBrace);
        }
    }
    return fres;
}

Parser::SimpleASTItem Parser::getSimpleAST(std::string::const_iterator& it, std::string::const_iterator end) {
    SimpleASTItem fres;
    fres.token = getToken(it, end);
    fres.type = SimpleASTItem::Type::token;
    // Check if list
    if (fres.token.type == Token::Type::character) {
        auto closingBrace = getClosingBrace(fres.token.data[0]);
        if (closingBrace) { // It's a list
            fres.type = SimpleASTItem::Type::list;
            // Parse through list
            while (true) {
                auto ASTItem = getSimpleAST(it, end);
                // Check that end is not reached
                if (it >= end) {
                    throw SyntaxError(std::string("Reached end while searching for terminating '")+closingBrace+"'");
                }
                // Check if list ends here
                if (ASTItem.type == SimpleASTItem::Type::token && ASTItem.token.data[0] == closingBrace) {
                    break;
                }
                // Add to children list
                fres.children.push_back(std::move(ASTItem));
            }
        }
    }
    // Return final result
    return fres;
}

Parser::SimpleASTItem Parser::getFullSimpleAST(std::string::const_iterator& it, std::string::const_iterator end) {
    SimpleASTItem fres;
    fres.token.data = " ";
    fres.type = SimpleASTItem::Type::list;
    while (it < end) {
        auto ast = getSimpleAST(it, end);
        if (!ast.token.data.empty()) {
            fres.children.push_back(ast);
        }
    }
    return fres;
}

std::unique_ptr<Parser::ASTItem> Parser::getAST(std::vector<SimpleASTItem>::const_iterator& it, std::vector<SimpleASTItem>::const_iterator end, ASTItem *parent) {
    auto origIt = it;
    auto fres = std::make_unique<ASTItem>();
    fres->parent = parent;
    fres->simpleAST = std::addressof(*it);
    // To be done in a scope or root
    if (parent->type | ASTItem::Type::scope || parent->type | ASTItem::Type::root) {
        // Check if scope
        if (it->type == SimpleASTItem::Type::list && it->getCharacter()== '{') {
            fres->type = ASTItem::Type::scope;
            fres->params = ASTItem::Scope{};
            it++;
        }
        // Check for types starting with identifier
        else if (it->type == SimpleASTItem::Type::token && it->token.type == Token::Type::identifier) {
            //TODO...
            it++;
        }
        // Check that Expression could be handled somehow
        else {
            throw SyntaxError("Unexpected token at start of scope: "+it->getHumanReadable(0));
        }
        // Check for semicolon
        if (it->type == SimpleASTItem::Type::token && it->token.type == Token::Type::character && it->token.data[0] == ';') {
            lastWasSemicolon = true;
            it++;
        }
    }
    // Work through children
    for (auto child = origIt->children.cbegin(); child < origIt->children.cend();) {
        fres->children.push_back(getAST(child, origIt->children.cend(), fres.get()));
    }
    // Return final result
    return fres;
}

std::unique_ptr<Parser::ASTItem> Parser::getFullAST(const SimpleASTItem& simpleASTRootItem) {
    auto fres = std::make_unique<ASTItem>();
    fres->type = ASTItem::Type::root;
    for (auto child = simpleASTRootItem.children.cbegin(); child < simpleASTRootItem.children.cend();) {
        fres->children.push_back(getAST(child, simpleASTRootItem.children.cend(), fres.get()));
    }
    return fres;
}

void Parser::parse(std::string::const_iterator& it, std::string::const_iterator end) {
    // Build simple AST
    rootSimpleASTItem = getFullSimpleAST(it, end);
    //std::cout << rootSimpleASTItem.getHumanReadable(-1) << std::endl;
    // Build real AST
    rootASTItem = getFullAST(rootSimpleASTItem);
}
}
