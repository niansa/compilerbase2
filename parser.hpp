#ifndef _PARSER_HPP
#define _PARSER_HPP
#include "symbol.hpp"
#include "types.hpp"

#include <string>
#include <vector>
#include <unordered_map>
#include <variant>
#include <memory>
#include <stdexcept>



namespace Compiler {
class Parser {
public:
    struct SyntaxError : public std::runtime_error::runtime_error {
        using std::runtime_error::runtime_error;
    };
    struct BadType : public std::runtime_error::runtime_error {
        using std::runtime_error::runtime_error;
    };

    struct Token {
        std::string_view data;
        enum class Type {
            character,
            identifier,
            string
        } type;

        std::string getHumanReadable() const;
    };
    struct SimpleASTItem {
        std::vector<SimpleASTItem> children;
        Token token;
        enum class Type {
            list,
            token
        } type;

        char getCharacter() const {
            return token.data[0];
        }

        std::string getHumanReadable(int depth) const;
    };
    struct ASTItem {
        struct Type {
            static constexpr uint8_t
                invalid =              0b00000000,
                expression =           0b01000000,
                value =                0b01000001,
                accessor =             0b01000011,
                constant =             0b01000101,
                declaration =          0b01001000,
                variable_declaration = 0b01001001,
                function_declaration = 0b01001010,
                keyword =              0b01010000,
                variable_definition =  0b01100000,
                scope =                0b10000000,
                executable_scope =     0b10000001,
                root  =                0b10000010;
        };
        struct Scope {
            std::unordered_map<std::string, DataSymbol> dataSymbols;
        };
        struct Value {
            DataSymbol *symbol;
        };
        struct VariableDeclaration {
            DataSymbol *provides;
        };
        struct FunctionDeclaration {
            ExecutableSymbol *provides;
        };

        const SimpleASTItem *simpleAST = nullptr;
        std::vector<std::unique_ptr<ASTItem>> children;
        ASTItem *parent = nullptr;
        uint8_t type = Type::invalid;
        std::variant<Scope, Value, VariableDeclaration, FunctionDeclaration> params;

        DataSymbol *findInScope(const std::string& name) {
            ASTItem *item = this;
            while (item) {
                if (item->type | Type::scope) {
                    auto& symbols = std::get<Scope>(item->params);
                    auto fres = symbols.dataSymbols.find(name);
                    if (fres != symbols.dataSymbols.end()) {
                        return std::addressof(fres->second);
                    }
                }
            }
            return nullptr;
        }
        DataSymbol *createSymbolInScope(const std::string& name, const DataSymbol& symbol) {
            ASTItem *item = this;
            while (item) {
                if (item->type | Type::scope) {
                    auto& symbols = std::get<Scope>(item->params);
                    return std::addressof(symbols.dataSymbols.emplace(name, DataSymbol{}).first->second);
                }
            }
            return nullptr;
        }
    };

    SimpleASTItem rootSimpleASTItem;
    std::unique_ptr<ASTItem> rootASTItem;
    std::vector<std::shared_ptr<VariableType>> types;
    std::unordered_map<std::string, DataSymbol> constDataSymbols;
    std::unordered_map<std::string, DataSymbol> dataSymbols;
    std::unordered_map<std::string, ExecutableSymbol> executableSymbols;
    bool lastWasSemicolon = false;

private:
    static char getClosingBrace(char c);
    static std::string_view getRawToken(std::string::const_iterator& it, std::string::const_iterator end);
    static std::string_view getString(std::string::const_iterator& it, std::string::const_iterator end, char terminator = '"');
    static Token getToken(std::string::const_iterator& it, std::string::const_iterator end);
    static SimpleASTItem getSimpleAST(std::string::const_iterator& it, std::string::const_iterator end);
    static SimpleASTItem getFullSimpleAST(std::string::const_iterator& it, std::string::const_iterator end);
    std::unique_ptr<ASTItem> getAST(std::vector<SimpleASTItem>::const_iterator& it, std::vector<SimpleASTItem>::const_iterator end, ASTItem *parent);
    std::unique_ptr<ASTItem> getFullAST(const SimpleASTItem& simpleASTRootItem);

public:
    Parser() {
        // Build built-in types
        types = BuiltinTypes::makeAll();
    }

    void parse(std::string::const_iterator& it, std::string::const_iterator end);

    std::shared_ptr<VariableType> getType(std::string_view name) {
        for (const auto& type : types) {
            if (type->name == name) {
                return type;
            }
        }
        throw BadType("Undefined type: "+std::string(name));
    }
};
}
#endif
