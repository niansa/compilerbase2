namespace Compiler {
struct VariableType;
struct FunctionType;
struct Symbol;
}

#ifndef _SYMBOL_HPP
#define _SYMBOL_HPP
#include "types.hpp"

#include <cstdint>
#include <vector>


namespace Compiler {
struct Symbol {
    off_t offset = 0;
    enum class Kind {
        data,
        executable
    } kind;
    std::shared_ptr<GeneralType> type;
};

struct DataSymbol : public Symbol {
    std::vector<char> initializer;
    bool isConst = false;
    Symbol *reference = nullptr;
    static inline Symbol *awaitingSymbol = reinterpret_cast<Symbol *>(0x1);

    DataSymbol() {
        kind = Kind::data;
    }

    std::string getHumanReadable(const std::shared_ptr<Symbol>& symbol) const {
        return (isConst?"const ":"")+type->getHumanReadable()+(reference?"&":"");
    }
};

struct ExecutableSymbol : public Symbol {
    ExecutableSymbol() {
        kind = Kind::executable;
    }
};
}
#endif
