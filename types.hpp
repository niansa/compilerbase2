namespace Compiler {
struct GeneralType;
struct VariableType;
struct FunctionType;
}

#ifndef _TYPES_HPP
#define _TYPES_HPP
#include <cstdint>
#include <string>
#include <string_view>
#include <vector>
#include <memory>
#include <utility>


namespace Compiler {
struct GeneralType {
    size_t size;

    virtual std::string getHumanReadable() const = 0;
};

struct VariableType : public GeneralType {
    std::string_view name;

    std::string getHumanReadable() const override {
        return std::string(name);
    }
};

struct FunctionType : public GeneralType {
    std::shared_ptr<GeneralType> returnType;
    std::vector<std::pair<std::shared_ptr<GeneralType>, std::string>> arguments;

    std::string getHumanReadable() const override {
        std::string fres = (returnType ? returnType->getHumanReadable()+' ' : "void ")+'(';
        for (const auto& [type, name] : arguments) {
            fres += type->getHumanReadable() + ' ' + name + ", ";
        }
        fres.pop_back();
        fres.back() = ')';
        return fres;
    }
};

namespace BuiltinTypes {
template<typename CppType>
struct BaseType : public VariableType {
    static CppType helperStatic; // Don't access! Just a little hack for figuring out the size

    CppType parseInitializer(std::string::const_iterator& it) {
        CppType fres = 0;
        while (isdigit(*it)) {
            if (fres != 0) {
                fres *= 10;
            }
            fres += *it - '0';
        }
        return fres;
    }
};

using Int8 = BaseType<int8_t>;
using UInt8 = BaseType<uint8_t>;
using Int16 = BaseType<int16_t>;
using UInt16 = BaseType<uint16_t>;
using Int32 = BaseType<int32_t>;
using UInt32 = BaseType<uint32_t>;
using Int64 = BaseType<int64_t>;
using UInt64 = BaseType<uint64_t>;

using Int = Int32;
using UInt = UInt32;

inline
auto make(std::string_view name) {
    std::shared_ptr<VariableType> abstract_fres = nullptr;
#define TYPEMAKE_10345673475(n) \
    if (name == #n) { \
        auto fres = std::make_shared<n>(); \
        fres->name = name; \
        fres->size = sizeof(fres->helperStatic); \
        abstract_fres = fres; \
    }
    TYPEMAKE_10345673475(Int)
    else TYPEMAKE_10345673475(UInt)
    else TYPEMAKE_10345673475(Int8)
    else TYPEMAKE_10345673475(UInt8)
    else TYPEMAKE_10345673475(Int16)
    else TYPEMAKE_10345673475(UInt16)
    else TYPEMAKE_10345673475(Int32)
    else TYPEMAKE_10345673475(UInt32)
    else TYPEMAKE_10345673475(Int64)
    else TYPEMAKE_10345673475(UInt64)
#undef TYPEMAKE_10345673475
    else if (name == "Void") {
        auto fres = std::make_shared<VariableType>();
        fres->name = "Void";
        fres->size = 0;
        abstract_fres = fres;
    }
    return abstract_fres;
}

inline
auto makeAll() { // This function could be a lot more efficient
    std::vector<std::shared_ptr<VariableType>> fres;
    // Special types
    for (const std::string_view name : {"Void", "Int", "UInt"}) {
        fres.push_back(make(name));
    }
    // Integer types with length
    for (const unsigned bitlength : {8, 16, 32, 64}) {
        fres.push_back(make("Int"+std::to_string(bitlength)));
        fres.push_back(make("UInt"+std::to_string(bitlength)));
    }
    return fres;
}
}
}
#endif
